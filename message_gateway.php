<?php

class Message {
    public function __construct(
      private string $user_id,
      private string $social_network_id,
      private int $date_added,
      private string $data_id,
      private string $data){}
}

class MessageGateway {
    static $handledPlatforms = ['facebook', 'twitter'];

    function normalizeMessage($JSONMessage, string $social_network_id) {
    $decodedMessage = json_decode($JSONMessage);

    if (! in_array($social_network_id, $this::$handledPlatforms)){
      throw new Exception('This platform is not supported!');
    }

    return match ($social_network_id) {
      'facebook' => new Message(
        $decodedMessage->sender->id,
        $social_network_id,
        $decodedMessage->timestamp,
        $decodedMessage->message->mid,
        $decodedMessage->message->text),
      'twitter' => new Message(
        $decodedMessage->message_create->sender_id,
        $social_network_id,
        $decodedMessage->created_timestamp,
        $decodedMessage->id,
        $decodedMessage->message_create->message_data->text,
      )};
    }

  function saveToDB(Message $message){}

  function saveToCache(Message $message){}

  function sendToAIAnalysis(Message $message){}

  function updateSockets(Message $message){}
}