<?php

include './message_gateway.php';

$facebookMessage = file_get_contents('./facebook_message.json');
$twitterDM = file_get_contents('./twitter_dm.json');

function messageHandler($json, $platform) {
    $messageGateway = new MessageGateway();

    try {
       $normalizedMessage = $messageGateway->normalizeMessage($json, $platform);

       $messageGateway->saveToDB($normalizedMessage);
       $messageGateway->saveToCache($normalizedMessage);
       $messageGateway->sendToAIAnalysis($normalizedMessage);
       $messageGateway->updateSockets($normalizedMessage);

       echo "$platform's captured message successfuly proccessed!";
    }
    catch (Exception $e) {
        echo "Message processing failed: {$e->getMessage()}";
    }
}

messageHandler($facebookMessage, 'facebook');